import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyBTIuOX0mehsGlta6JXssLvHQg9ByBsiqU",
    authDomain: "winecchat.firebaseapp.com",
    databaseURL: "https://winecchat.firebaseio.com",
    projectId: "winecchat",
    storageBucket: "winecchat.appspot.com",
    messagingSenderId: "984474675764",
    appId: "1:984474675764:web:5d6e046a75301777b77861"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;
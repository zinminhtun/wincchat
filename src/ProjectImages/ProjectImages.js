const Images = {
    wc:require('../images/winecpeople.jpg'),
    nopic: require('../images/nopic.jpeg'),
    choosefile: require('../images/ic_input_file.png'),
    input_file: require('../images/ic_photo.png'),
    sticker: require('../images/ic_sticker.png'),
    send: require('../images/ic_send.png'),
    lego1: require('../images/sticker1.png'),
    lego2: require('../images/sticker2.png'),
    lego3: require('../images/sticker3.png'),

}

export default Images
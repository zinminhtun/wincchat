import React, {Component} from 'react';
import Header from '../../Components/Header';
import Footer from '../../Components/Footer';
import './Home.css';
import images from '../../ProjectImages/ProjectImages';
import {Link} from 'react-router-dom'

export default class Home extends Component{
    render(){
        return(
            <>
            <Header/>
                    <div class="splash-container">
                        <div class="splash">
                            <h1 class="splash-head">WEB CHAT APP</h1>
                            <p class="splash-subhead">
                                Let's talk with our loved ones
                            </p>
                            <div id="custom-button-wrapper">
                                <Link to = '/login'>
                                    <a className="my-super-cool-btn">
                                        <div className="dots-container">
                                            <div className="dot"></div>
                                            <div className="dot"></div>
                                            <div className="dot"></div>
                                            <div className="dot"></div>
                                        </div>
                                        <span className="buttoncooltext">Get Started</span>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div class="content-wrapper">
                        <div class="content">
                            <h2 class="content-head is-center"> Features of WebChat Application</h2>

                            <div className="Appfeatures">
                                <div className="contenthead">
                                    <h3 class="content-subhead">
                                        <i class="fa fa-rocket"></i>
                                        Get Started Quickly
                                    </h3>
                                    <p>
                                        Just register yourself with this app and start chating with your loved ones
                                    </p>
                                </div>
                                <div class="1-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
                                    <h3 class="content-subhead">
                                        <i class="fa fa-sign-in"></i>
                                        Firebase Authentication
                                    </h3>
                                    <p>
                                        Firebase Authentication has been implemented in this app
                                    </p>
                                </div>
                                <div class="1-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
                                    <h3 class="content-subhead">
                                        <i class="fa fa-th-large"></i>
                                        Media
                                    </h3>
                                    <p>
                                        You can share images with your friends for better experience
                                    </p>
                                </div>
                                <div class="1-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
                                    <h3 class="content-subhead">
                                        <i class="fa fa-refresh"></i>
                                        Updates
                                    </h3>
                                    <p>
                                        We will working with new features for this app for better experienc in future
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="AppfeaturesFounder">
                            <div class="1-box-lrg is-center pure-u-1 pure-u-md-1-2 pure-u-lg-2-5">
                                <img width="300" alt="File Icons" class="pure-img-responsive" src={images.wc}/>
                            </div>
                            <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-2-5" style={{marginLeft:'13px'}}>
                                <h2 class="content-head content-head-ribbon">WineC Orginzation</h2>

                                <p style={{color:'white'}}>
                                    Perfect Orginzation
                                </p>
                                <p style={{color: 'white'}}>
                                    Currently inventing social media website and busy to explore new ideas with new technologies being developed software.
                                </p>
                            </div>
                        </div>

                        <div class="content">
                            <h2 class="content-head is-center">Who We Are?</h2>
                            <div class="Appfeatures">
                                <div class="1-box-lrg pure-u-1 pure-u-md-2-5">
                                    <form class="pure-form pure-form-stacked">
                                            <label for="name">Your Name</label>
                                            <input id="name" type="text" placeholder="Your Name"/>
                                            <label for="email">Your Email</label>
                                            <input id="email" type="email" placeholder="Your Email"/>

                                            <label for="password">Your Password</label>
                                            <input id="password" type="password" placeholder="Your Password"/>

                                            <button type="submit" class="pure-button">Sign Up</button>
                                   
                                    </form>
                                </div>

                                <div class="1-box-1rg pure-u-1 pure-u-md-3-5">
                                    <h4>Contact Us</h4>
                                    <p>
                                        For any question or suggestion you can directly contact us on our Fackbook Page:
                                        <a href="https://www.facebook.com/zin.htun.75098">https://www.facebook.com/zin.htun.75098</a>
                                    </p>
                                    <h4>More Information</h4>
                                    <p>
                                        To whom it may concern
                                    </p>
                                    <p>
                                        This App is developed for learning purpose - 
                                        Developed by Zin Min Htun 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <Footer/>
                    </div>
                    
            </>
        )
    }
}